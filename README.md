# Project & Portfolio 1 *!*

My name is Courtney Price, and I'm a Web Design & Development major at Full Sail University. This repository is to help better understand my works in coding as well as giving a better outlook, and informing about the changes I'm making in each code. Below, you will find updates on projects, notes about what I'm doing with the projects and possible codes I've worked on. 

  

## Jotting it Down *!*

```
 - Here will be updates on what I'm working on,
 and any changes made to it. 
 
 * 05/05/18 - ADDED SDI final project
 * 05/13/18 - ADDED P&P 1 coding project
      - updated readme.md 

 * 05/16/18 - UPDATED P&P 1 coding project
      - Completed SwapInfo, Backwards, and TempConvert
      -  updated readme.md
      -  ADDED SDI 'Count the Cart' assingment
              -Refactoring...  

* 05/22/18  - UPDATED P&P 1 coding project completed
      - All excerises within the coding challenge are completed.
```


## What's the Update *?*

```
Here are two websites to help you keep track on what 
I'm doing now, what's been done, and what I'm 
planning to do. 

* 05/13/18 
      - Added two files to Google Drive. Research thesis on Impact of C#, and Version Control 
information. 
      - New updates to Trello, more activities that were completed. Added incomplete
section. 
      - Added notes to completed and incompleted blocks. 

* 05/20/18
      - Added file to Google Drive. Agile Research Paper.
      - New Updates on Trello, activities for week 3 completed. 

* 05/22/18
      - Updating Trello, adding sprints, and notes to completed assignments.
      - To turn in Final Exam.

```
* [Trello](https://trello.com/b/tS1195et/2018maypricecourtneydvp1storyboard)
* [Google Drive](https://drive.google.com/open?id=1-SdQei063UCNqYoCMbkhsdxZoqP43GtF)



## Changes in Projects *!*

```
Any changes, difficulties, solutions, etc will be 
added here.

* 05/13/15 - P&P 1 coding project updates
       - finished problem one 'swapinfo'.
       - refreshed memory on Console.WriteLine();, Console.ReadLine();, validating using
While loops such as while(string.IsNullorEmptySpace){}, or while(string.IsEmpty){}. 
       - What I found difficult: Reserve method. Something I learned while doing the 
first problem. Used Array.Reverse method to reverse order of name displayed to user. 
       - Notes for 'swapinfo': Look more into Reverse methods, and familiar. Also, do more
research on classes to be more familiar when making them. 

* 05/16/18 - P&P 1 coding project updates
       - finished 'backwards' and 'tempconvert'
       - process of completing 'age convert'
       - Reverse method became more easier to use even though it was a bit different
than the one in 'swapinfo'.  
       - 'tempConvert' was simple, but in need to look back on rounding method in c#.
       - 'Age Convert' is giving me a bit of trouble so it's incomplete for the time being.
Self-Reminder: Go back to challenge later.

* 05/16/18 - SDI 'Count the Cart' updates
       - Refactoring. Found that 'for loops' work rather than 'if statements'.
       - To find how to make results show for one item in the cart, rather than
all items in the cart. 

* 05/16/18 - P&P 1 coding project updates
       - Found that AgeConvert and Menu challeges were the most difficult.
       - For age convert I decided to do everything manually as it was the easier
approach for me to do. Rather than using DateTime as nothing was being hardcoded. 
I think that is why it took the most time for me to complete this challenge as I was trying
to work on something that I was not familiar with. 
       - Menu challenge proved to the a little bit more challenging than AgeConvert. 
Using do-while loop proved more useful in getting the menu to reappear after the user
completes each challenge.
```



## Other *?*

```
*work on time management, and finding more time to do work and research.*

           - Finish Agile research paper, and first half of progress report // COMPLETED.
           - Finish 'Age Convert' challenge and start on 'menu' challenge. // COMPLETED.
           - add SDI files and refactor.

```








