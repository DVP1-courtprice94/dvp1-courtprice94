﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Price_Courtney_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Courtney Price
             March 19, 2018
             Section 02
             Final Project
             */

            // FUNDRAISER -- CAKE WALK

            //WELCOME USER AN EXPLAIN TO THE USER THE PURPOSE OF THE PROGRAM

            Console.WriteLine("Welcome user to Cake Walk. In this program we will be calculating the profit that you will make based on the cakes you sell as a whole, slicing them, and selling into individual pieces.\r\n");

            //PROMPT USER FOR TEXT STRING OF CAKE TYPES 
            Console.WriteLine("Please enter the cakes you're going to buy.");
            string cakeName = Console.ReadLine();
            //VALIDATE 
            while (string.IsNullOrEmpty(cakeName))
            {
                Console.WriteLine("Do not leave blank");
                cakeName = Console.ReadLine();
            }

            //SPLIT TEXT STRING OF NAMES INTO STRING ARRAY
            string[] cakeArray = cakeName.Split(',' );

            //RETURN PROMPTFORCAKECOST BACK TO MAIN
            decimal[] cakeCost = PromptForCakeCost(cakeArray);
            for(int i=0; i<cakeCost.Length; i++)
            {
                Console.WriteLine("The {0} cake is {1}", cakeArray[i].Trim(), cakeCost[i].ToString("C"));
            }

            //RETURN TOTALCAKECOST BACK TO MAIN 
            decimal grandTotalCost = TotalCakeCost(cakeCost);

            //ASK USER FOR NUMBER OF SLICES OF CAKE, AND COST FOR ONE SLICE
            Console.WriteLine("How many slices will your cake be cut into?");
            string cakeSlice = Console.ReadLine();
            //VALIDATE
            int slice;
            if(!int.TryParse(cakeSlice, out slice))
            {
                Console.WriteLine("Please do not leave blank.");
                cakeSlice = Console.ReadLine();
            }

            Console.WriteLine("What is the cost for one slice of cake?");
            string sliceCost = Console.ReadLine();
            //VALIDATE
            decimal sCost;
            if(!decimal.TryParse(sliceCost, out sCost))
            {
                Console.WriteLine("Please do not leave blank.");
                sliceCost = Console.ReadLine();
            }

            //RETURN AMOUNTSOLDFOR TO MAIN
            decimal amount = AmountSoldFor(cakeArray, slice, sCost);
            int totalSlice = cakeArray.Length * slice;

            //CALCULATE THE PROFIT THE USER WILL MAKE IF THEY SELL EACH SLICE OF CAKE
            decimal sliceCakeTotal = amount - grandTotalCost;
            Console.WriteLine("\r\nYour foundation will make {0}, if you sell all {1} cakes sliced into {2} pieces for {3} per slice.", sliceCakeTotal.ToString("C"), cakeArray.Length, totalSlice, sCost.ToString("C"));
            
        }

        //CREATE CUSTOM FUNCTION FOR PROMPTFORCAKECOST
        public static decimal[] PromptForCakeCost(string[] cakeArray)
        {

            decimal[] cakeCost = new decimal[cakeArray.Length];
            //LOOP THROUGH CAKE ARRAY, AND PROMPT USER FOR COST OF EACH CAKE.
            for (int i = 0; i < cakeArray.Length; i++)
            {
                Console.WriteLine("What is the cost of {0}?", cakeArray[i].Trim());
                string cost = Console.ReadLine();
                //VALIDATE
                decimal c;
                if (!decimal.TryParse(cost, out c))
                {
                    Console.WriteLine("Please do not leave blank.");
                    cost = Console.ReadLine();

                }
                decimal cakecost = Convert.ToDecimal(cost);
                cakeCost[i] = cakecost;         

            }
            Console.WriteLine("\r\n");
           
            return cakeCost;

        }
        //END PROMPTFORCAKECOST FUNCTION

        //CREATE FUNCTION FOR TOTALCAKECOST
        public static decimal TotalCakeCost(decimal[] cakeCost)
        {
            //CREATE A VARIABLE TO HOLD THE SUM OF ALL CAKE
            decimal grandCakeTotal=0;

            //CREATE A LOOP TO ADD AND GET TOTAL COST OF ALL CAKES
            for(int i=0; i<cakeCost.Length; ++i)
            {
                grandCakeTotal += cakeCost[i];
            }
            Console.WriteLine("\r\nThe total of all the cakes is {0}", grandCakeTotal.ToString("C"));
            return grandCakeTotal;
        }
        //END TOTALCAKECOST FUNCTION 

        //CREATE FUNCTION FOR AMOUNTSOLDFOR
        public static decimal AmountSoldFor(string[] cakeArray, int slice, decimal sliceCost)
        {
            decimal amount = (cakeArray.Length * slice) * sliceCost;
            Console.WriteLine("The total amount is {0}\r\n", amount.ToString("C"));
            return amount;
        }

        /*
         TEST ONE 
         CAKE NAMES - "DOUBLE CHOCOLATE, WHITE CHOCOLATE, LEMON SWIRL"
         CAKE PRICES:
            1. THE DOUBLE CHOCOLATE CAKE IS $12.00
            2. THE WHITE CHOCOLATE CAKE IS $12.50
            3. THE LEMON SWIRL CAKE IS $14.50
        COST OF ALL CAKES IS $42.00
        NUMBER OF SLICES PER CAKE - 8
        COST OF 1 SLICE OF CAKE - $5.00
        AMOUNT MADE IS $120.00
        FINAL OUTPUT:
        "YOUR FOUNDATION WILL MAKE $78.00, IF YOU SELL ALL 3 CAKES SLICED INTO 24 PIECES FOR $5.00 PER SLICE." 

        TEST TWO
        CAKE NAMES - "CHOCOLATE, VANILLA, RED VELVET"
        CAKE PRICES:
            1. THE CHOCOLATE CAKE IS $10.00
            2. THE VANILLA CAKE IS $10.00
            3. THE RED VELVET CAKE IS $15.00
        COST OF ALL CAKES IS $35.00
        NUMBER OF SLICES PER CAKE - 5
        COST OF 1 SLICE OF CAKE - $2.00
        AMOUNT MADE IS $30.00
        FINAL OUTPUT:
        "YOUR FOUNDATION WILL MAKE ($5.00), IF YOU SELL ALL 3 CAKES SLICED INTO 15 PIECES FOR $2.00 PER SLICE."

        TEST THREE 
        CAKE NAMES - "CHOCOLATE, VANILLA, RED VELVET, ICE CREAM"
        CAKE PRICES:
            1. THE CHOCOLATE CAKE IS $10.00
            2. THE VANILLA CAKE IS $10.00
            3. THE RED VELVET CAKE IS $15.00
            4. THE ICE CREAM CAKE IS $16.50
        COST OF ALL CAKES IS $51.50
        NUMBER OF SLICES PER CAKE - 8
        COST OF 1 SLICE OF CAKE - $8.00
        AMOUNT MADE IS $256.00
        FINAL OUTPUT:
        "YOUR FOUNDATION WILL MAKE $204.50 , IF YOU SELL ALL 4 CAKES SLICED INTO 32 PIECES FOR $8.00 PER SLICE."

        TEST FOUR 
        CAKE NAMES - "CHOCOLATE, VANILLA, RED VELVET, ICE CREAM, POUND"
        CAKE PRICES:
            1. THE CHOCOLATE CAKE IS $12.00
            2. THE VANILLA CAKE IS $12.00
            3. THE RED VELVET CAKE IS $15.00
            4. THE ICE CREAM CAKE IS $16.50
            5. THE POUND CAKE IS $12.00
        COST OF ALL CAKES IS $67.50
        NUMBER OF SLICES PER CAKE - 5
        COST OF 1 SLICE OF CAKE - $8.00
        AMOUNT MADE IS $200.00
        FINAL OUTPUT:
        "YOUR FOUNDATION WILL MAKE $132.50, IF YOU SELL ALL 5 CAKES SLICED INTO 25 PIECES FOR $8.00 PER SLICE."


         */

    }
}
    

