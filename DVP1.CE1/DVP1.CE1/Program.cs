﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Name: Courtney Price
             Date: 1805
             Course: Project & Portfolio 1
             Synopsis: A collection of coding excercises that will help broaded my knowledge on C# by the end of this month.
             
             */
             

            Menu.MyMenu(); //Create a menu that will let user choose which challege they want to do.
            SwapInfo.MySwap(); //Using user prompted information, send back their name in reversed order.
            Backwards.MyBackwards(); // Using user prompted information, have the user enter a sentence with six or more words and send it back backwards.
            AgeConvert.MyAgeConvert(); //Using user prompted information, have user enter their name and age and calculated. Display the output showing the user the days, hours, mins, and seconds they've been alive.
            TempConvert.MyTempConvert(); //Using user prompted information, convert temperture user enter from Farenheit to Celsius, then convert Celsius back to Farenheit.
      
        }
    }
}
