﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public static class SwapInfo
    {
       static public void MySwap()
        {
            /*
             Name: Courtney Price
             Date: 1805
             Course: Project & Portfolio 1
             Synopsis: Using user prompted information, send back their name in reversed order.
             */

            //Prompting user for first name only.
            Console.WriteLine("Hello, please enter your first name.");
            string firstName = Console.ReadLine();

            //Validating if prompted information is left blank or not.
            while (string.IsNullOrEmpty(firstName))
            {
                Console.WriteLine("Please do not leave this blank. Please enter your first name.");
                firstName = Console.ReadLine();
            }

            //Display the first name that the user inputed
            Console.WriteLine("Your first name is " + firstName+". \r\n");

            //Prompting user for last name only.
            Console.WriteLine("Now, please enter your last name.");
            string lastName = Console.ReadLine();

            //Validating if prompted information is left blank or not.
            while (string.IsNullOrEmpty(lastName))
            {
                Console.WriteLine("Please do not leave this blank. Please enter your last name.");
                    lastName = Console.ReadLine();
            }

            //Display the last name the user inputed.
            Console.WriteLine("Your last name is " + lastName + ". \r\n");

            //Reversing order of the first name.
            string firstOne = "Your first name is " + firstName + ".";

            string[] words = firstOne.Split(' ');
            Array.Reverse(words);
            firstOne = String.Join(" ", words);

            //Display the reverse order of the first name.
            Console.WriteLine(firstOne+"\r\n");

            //Reversing order of the last name.
            string secondOne = "Your last name is " + lastName + ".";

           
            string[] wordsTwo = secondOne.Split(' ');
            Array.Reverse(wordsTwo);
            secondOne = String.Join(" ", wordsTwo);

            //Display the reverse order of the last name.
            Console.WriteLine(secondOne+"\r\n");

            //SwapInfo end.
        }
    }
}
