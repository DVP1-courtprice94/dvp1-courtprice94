﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class AgeConvert
    {
        static public void MyAgeConvert()
        {
            /*
            Name: Courtney Price
            Date: 1805
            Course: Project & Portfolio 1
            Synopsis: Using user prompted information, have user enter their name and age and calculated. Display the output showing the user the days, hours, mins, and seconds they've been alive.
            */

            //Prompt user for name and age.

            Console.WriteLine("Hello, what is your name?");
            string whoYou = Console.ReadLine();

            Console.WriteLine("What's your age?");
            string userAge = Console.ReadLine();

            //Display name and age back to the user
            Console.WriteLine("Your name is {0}, and you're {1} years old.", whoYou, userAge);

            //Convert age to int.
            int age;
            int.TryParse(userAge, out age);

            //Calculating days, hours, minutes, and seconds ( not leap year )
            int getDays = 365 * age;
            int getHours = 8760 * age;
            int getMinutes = 525600 * age;
            int getSeconds = 31536000 * age;

            //Calculating days, hours, minutes, and seconds IF leap year
            int getLeapDays = 366 * age;
            int getLeapHours = 8784 * age;
            int getLeapMinutes = 527040 * age;
            int getLeapSeconds = 31622400 * age;

            //Displaying information back to user

            Console.WriteLine("{0} who is now {1} years old. It has been {2} days, {3} hours, {4} minutes, and {5} seconds since you've been born.", whoYou, userAge, getDays, getHours, getMinutes, getSeconds);

            Console.WriteLine("Wait! Were you born in a leap year? Well, {0} who is now {1} years old. It has been {2} days, {3} hours, {4} minutes, and {5} seconds since you've been born.", whoYou, userAge, getLeapDays, getLeapHours, getLeapMinutes, getLeapSeconds);
            
            //AgeConvert end.

        }

      
    }
}
