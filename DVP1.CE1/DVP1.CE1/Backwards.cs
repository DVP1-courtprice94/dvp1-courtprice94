﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Backwards
    {
        static public void MyBackwards()
        {
            /*
            Name: Courtney Price
            Date: 1805
            Course: Project & Portfolio 1
            Synopsis: Using user prompted information, have the user enter a sentence with six or more words and send it back backwards.
            */

            //Prompt the user for a sentence that has six or more words.
            Console.WriteLine("Please enter a sentence that includes six or more words in it.");
            string sentenceSix = Console.ReadLine();

            string[] words = sentenceSix.Split(' ');

            //Validate to see if user has entered six or more words in the sentence and validate if the user did not leave blank. 
            while (string.IsNullOrEmpty(sentenceSix))
            {
                Console.WriteLine("Please do not leave blank.");
                sentenceSix = Console.ReadLine();
            }
            if (words.Length < 6)
            {
                Console.WriteLine("Not enough words were entered for this sentence. Please write a sentence with six or more words.");
                sentenceSix = Console.ReadLine();
            }

            //Using the information given, reverse it so that it outputs to the user in reverse order.


            char[] reverseSentece = sentenceSix.ToCharArray();
            Array.Reverse(reverseSentece);
            Console.WriteLine(reverseSentece+"\r\n");

            //Backwards end.
            
        }
    }
}
