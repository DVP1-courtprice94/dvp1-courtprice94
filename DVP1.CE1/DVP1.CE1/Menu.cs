﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Menu
    {
        /*
             Name: Courtney Price
             Date: 1805
             Course: Project & Portfolio 1
             Synopsis: Create a menu that will let user choose which challege they want to do.
             */
        static public void MyMenu()
        {
            //Do-While loop that will go back to menu once challenge is done.
            int selected = 0;

            do
            {
                selected = MenuSelect();
            } while(selected!=5);
            
        }

        //Method for menu creation. Return will let the user choose another option once challenge is completed.
        public static int MenuSelect()
        {
            //Display Menu to user 
            Console.WriteLine("Hello, and welcome to the C# Challenge Menu. Which challenge would you like to do? ");
            Console.WriteLine("1: SwapInfo Challenge: Using user prompted information, send back their name in reversed order.");
            Console.WriteLine("2: Backwards Challenge: Using user prompted information, have the user enter a sentence with six or more words and send it back backwards.");
            Console.WriteLine("3: AgeConvert Challenge:  Using user prompted information, have user enter their name and age and calculated. Display the output showing the user the days, hours, mins, and seconds they've been alive.");
            Console.WriteLine("4: TempConvert Challenge: Using user prompted information, convert temperture user enter from Farenheit to Celsius, then convert Celsius back to Farenheit.");
            Console.WriteLine("5: Exit: Return to menu");
            //User input number choice.
            string userSelect = Console.ReadLine();

            Console.WriteLine("\r\n");
            //Converting string to int
            int select;
            int.TryParse(userSelect, out select);

            //Conditional statements for challenge. When user types in number the choice will go to the selected challenge.
            if (select == 1)
            {
                SwapInfo.MySwap();
            }if (select == 2)
            {
                Backwards.MyBackwards();
            }
            if (select == 3)
            {
                AgeConvert.MyAgeConvert();
            }
            if (select == 4)
            {
                TempConvert.MyTempConvert();
            }

            Console.WriteLine("\r\n");

            return select;

            //Menu challenge end.
        }
    }
}
