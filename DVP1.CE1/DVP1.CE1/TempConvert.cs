﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class TempConvert
    {
        static public void MyTempConvert()
        {
            /*
             Name: Courtney Price
             Date: 1805
             Course: Project & Portfolio 1
             Synopsis: Using user prompted information, convert temperture user enter from Farenheit to Celsius, then convert Celsius back to Farenheit.
             */

            //Prompt user for a temperature in Farenheit.
            Console.WriteLine("Please enter a temperature using Farenheit.");
            string tempFaren = Console.ReadLine();

            //Converting string to decimal
            decimal tempF;
            decimal.TryParse(tempFaren, out tempF);

            //Calculating Farenheit to Celsius
            decimal toCelsius = (tempF - 32) * 5 / 9;
            

            //Output Farenheit to Celsius
            Console.WriteLine(tempFaren+" degrees farenheit is "+Math.Round(toCelsius)+" degrees celsius");

            //Prompt user for a temperature in Celsius.
            Console.WriteLine("Please enter a temperature using Farenheit.");
            string tempCelsi = Console.ReadLine();

            //Converting string to decimal
            decimal tempC;
            decimal.TryParse(tempCelsi, out tempC);

            //Calculating Celsius to Farenheit
            decimal toFaren = tempC * 9 / 5 + 32;

            //Output Celsius to Farenheit
            Console.WriteLine(tempCelsi + " degrees celsius is " + Math.Round(toFaren) + " degrees farenheit.");
            
            //TempConvert end.
        }
    }
}
