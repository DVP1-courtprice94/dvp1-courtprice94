﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Price_Courtney_CountCart
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Courtney Price
             March 11, 2018
             Section 02
             Count the Cart
             */

            // DECLARE AND DEFINE THE ARRAY

            string[] shopArray = new string[10] { "snack", "drink", "vegetable", "drink", "meat", "snack", "vegetable", "snack", "drink", "drink" };

            //PROMPT USER TO PICK NUMBER 1 THROUGHT 4 
            Console.WriteLine("Choose (1) for Snacks, (2) for Drinks, (3) for Vegetables, and (4) for Meats.");
            string inShop = Console.ReadLine();

            //VALIDATE TO SEE IF USER USES THE NUMBERS ASKED
            while (shopArray.Length < 4)
            {
                Console.WriteLine("Please enter a number and be sure to use numbers listed.");
                inShop = Console.ReadLine();
            }

           //Using for loop instead of if statments with breaks to search through menu.
                for (int i = 0; i < shopArray.Length; i++)
                {
                    int count = 0;
                    for (int a = 0; a < shopArray.Length; a++)
                    {
                        if (shopArray[i] == shopArray[a])
                            count = count + 1;
                    }
                Console.WriteLine("In the shopping cart there are " + count+" \t\n"+ shopArray[i]);
                
            }
          
        }
    }


}


